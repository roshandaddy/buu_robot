//
// Created by sunhao on 7/1/19.
//


#include <ros/ros.h>


#include "buu_controller/controller.hpp"

int main(int argc, char ** argv)
{
    ros::init(argc,argv,"controller");
    ros::NodeHandle nh,nh_para("~");

    buu_ctrller::Ctrller my_ctrller;

    /*   STEPS:
     *   1. subscribe topic "/com/veh_stat" with function Ctrller::callbackVehStat
     *   2. subscribe topic  "/com/path" with function Ctrller::callbackPath
     *   3. publish variable 'cmd' through topic "/com/serial"
     * */




    ros::Rate loop_rate(10);
    while ( ros::ok() )
    {
        ros::spinOnce();
        std_msgs::String cmd;
        my_ctrller.ctrl(cmd);
        loop_rate.sleep();
    }

    return 0;
}
