//
// Created by sunhao on 4/12/21.
//

#include <ros/ros.h>
#include <std_msgs/Int32.h>

int main(int argc, char** argv){
    ros::init(argc,argv,"talker_tnl");
    ros::NodeHandle nh, nh_para("~");
    auto puber = nh.advertise<std_msgs::Int32>("/rss/num",1);

    ros::Rate loop_rate(10);
    std_msgs::Int32 msg;
    int i = 0;
    while (ros::ok()){
        msg.data = i;
        puber.publish( msg );
        ROS_INFO("Send num: %d",msg.data);
        loop_rate.sleep();
        if (i<9){
            ++i;
        }else{
            i = 0;
        }
    }

    return 0;
}