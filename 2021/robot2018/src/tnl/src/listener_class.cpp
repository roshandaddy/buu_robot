//
// Created by sunhao on 4/12/21.
//



#include <ros/ros.h>

#include <sub_template.h>

int main(int argc, char** argv){
    ros::init(argc,argv,"listener_class");
    ros::NodeHandle nh, nh_para("~");
    auto suber = tnl::Suber(nh);
    suber.run();
    return 0;
}