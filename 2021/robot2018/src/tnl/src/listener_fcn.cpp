//
// Created by sunhao on 4/12/21.
//

#include <ros/ros.h>


#include "sub_template.h"


int main(int argc, char** argv){
    ros::init(argc,argv,"listener_fcn");
    ros::NodeHandle nh, nh_para("~");

    ros::Subscriber sub = nh.subscribe("/rss/num",1,tnl::callback);
    ros::spin();
    return 0;
}