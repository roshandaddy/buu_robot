//
// Created by sunhao on 5/11/21.
//


#include <iostream>

#include <ros/ros.h>
#include <std_msgs/Int32.h>

int main(int argc, char** argv){
    ros::init(argc,argv,"talker_param");
    ros::NodeHandle nh, nh_para("~");
    auto puber = nh.advertise<std_msgs::Int32>("/rss/num",1);

    int param;
    nh_para.param("num",param,10);

    ros::Rate loop_rate(10);
    std_msgs::Int32 msg;
    while (ros::ok()){
        msg.data = param;
        puber.publish( msg );
        std::cout << "get parameter:" << param << std::endl;
        loop_rate.sleep();
    }

    return 0;
}