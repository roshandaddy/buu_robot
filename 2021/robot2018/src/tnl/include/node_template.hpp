//
// Created by sunhao on 4/20/21.
//

#ifndef SRC_NODE_TEMPLATE_HPP
#define SRC_NODE_TEMPLATE_HPP


#include <std_msgs/String.h>
#include <std_msgs/Int32.h>

namespace tnl{

    class NodeTemp{
    public:
        explicit NodeTemp(ros::NodeHandle & nh){
//            sub_ = nh.subscribe("",1,,this);
//            puber_ = nh.ad
        }

        void run(){
            ros::Rate loop_rate(10);
            while(ros::ok()){
                ros::spinOnce(); //  callback
                doFcn(); // do the work
                pubMsg(); //  publish
                loop_rate.sleep();
            }

        }

    private:

        void callbackFloat(){

        }


        void pubMsg(){

        }

        void doFcn(){

        }

    private:
        ros::Subscriber  suber_;
        ros::Publisher puber_;


    };


}

#endif //SRC_NODE_TEMPLATE_HPP
