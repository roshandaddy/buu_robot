//
// Created by sunhao on 4/12/21.
//

#ifndef SRC_SUB_TEMPLATE_H
#define SRC_SUB_TEMPLATE_H

#include <ros/ros.h>
#include <std_msgs/Int32.h>


namespace tnl{

void callback(const std_msgs::Int32::ConstPtr & msg);

//----------------------------------

class Suber{
public:
    explicit Suber(ros::NodeHandle & nh){
        suber_ = nh.subscribe("/rss/num",1,&Suber::callback,this);
    }

    void run(){
        ros::Rate loop_rate(1);
        while(ros::ok()){
            ros::spinOnce();
            loop_rate.sleep();
        }
    }

private:
    void callback(const std_msgs::Int32::ConstPtr & msg);
    ros::Subscriber suber_;
};


}

#endif //SRC_SUB_TEMPLATE_H
