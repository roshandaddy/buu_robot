//
// Created by sunhao on 5/7/21.
//

#include <string>
#include <ros/ros.h>
#include <std_msgs/String.h>


#include "user_msgs/VehCfg.h"
#include "user_msgs/GPGGA.h"




double str2f(std::string tmp,bool flag)
{
    std::stringstream ss;
    int tmp_int;
    double tmp_data,cur_data;
    ss << tmp;
    ss >> tmp_data;//
}
//----------------------------------------------------
double str2f(std::string tmp)
{
    std::stringstream ss;
    int tmp_int;
    double tmp_data,cur_data;
    ss << tmp;
    ss >> tmp_data;//
    tmp_data = tmp_data / 100;
    tmp_int = (int)tmp_data;// get deg
    tmp_data = (tmp_data - tmp_int)*100/60;
    cur_data = tmp_int + tmp_data;
    return cur_data;
}
//----------------------------------------------------
int str2n(std::string tmp)
{
    int tmp_data;
    std::stringstream ss;
    ss << tmp;
    ss >> tmp_data;//
    return  tmp_data;
}

//----------------------------------------------------
void getGPGGAData(const std::string & buf,user_msgs::GPGGA & msg)
{

    //msg.alt = 0
    std::string str = buf;
    int str_len = str.size();
    std::vector<int> dot_cnt;
    for(int i=0;i<str_len;i++)
    {
        if( str[i]==',')
        {
            dot_cnt.push_back( i );
        }
    }

    int cur_cnt,st_cnt,end_cnt;
    std::string tmp="";

    //----utc---
    cur_cnt =1;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }

    double utc_tmp;
    utc_tmp = str2f( tmp, true );
    int h = (int) utc_tmp / 10000;
    int m = (int) utc_tmp / 100 - h*100;
    utc_tmp = utc_tmp - h*10000 - m*100;
    utc_tmp = utc_tmp + h*3600 + m*60;
    auto utc_seconds = utc_tmp;


    //----lat---
    cur_cnt =2;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto lat = str2f( tmp );

    msg.lat = lat;
    //----lat-char--
    cur_cnt =3;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto lat_dir = tmp;

    //----lon---
    cur_cnt =4;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto lon = str2f( tmp );


    //----lat-char--
    cur_cnt =5;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto lon_dir = tmp;

    //-------gps stat----------
    cur_cnt =6;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto gps_qual = str2n( tmp );

    //------- stat num----------
    cur_cnt =7;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto num_sats = str2n( tmp );


    //----alt---
    cur_cnt = 8;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto hdop = str2f( tmp,true );


    //----alt---
    cur_cnt =9;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto alt = str2f( tmp,true );

    //----alt---
    cur_cnt =10;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto altitude_units = tmp;


    //----alt---
    cur_cnt =11;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto undulation = str2f( tmp,true );

    //----alt---
    cur_cnt =12;
    st_cnt = dot_cnt[cur_cnt-1]+1;
    end_cnt = dot_cnt[cur_cnt]-1;
    tmp="";
    for( int i=st_cnt;i<=end_cnt;i++)
    {
        tmp = tmp+str[i];
    }
    auto undulation_units = tmp;
}


int main(int argc,char ** argv)
{
    ros::init(argc,argv,"send_GPGGA");
    ros::NodeHandle nh;

    ros::Publisher gpgga_puber;
    gpgga_puber = nh.advertise<user_msgs::GPGGA>("/simulation/GPGGA",2);
    std::string gpgga_str = "$GPGGA,121252.000,3937.3032,N,11611.6046,E,1,05,2.0,45.9,M,-5.7,M,,0000*77";
    //----------------

    std::string send_str;
    std_msgs::String send_data;
    ros::Rate loop_rate(5);
    send_data.data = gpgga_str;


    user_msgs::GPGGA msg;
    while( ros::ok() )
    {
        getGPGGAData(gpgga_str,msg);
        gpgga_puber.publish(msg);
        loop_rate.sleep();
    }

    return 0;
}