//
// Created by sunhao on 4/6/21.
//

#include "ros/ros.h"


int main(int argc, char** argv){
    ros::init(argc,argv,"hello_world_from_ros");
    ros::NodeHandle nh,nh_para("~");

    ros::Rate loop_rate(1);
    while(ros::ok()){
        ROS_ERROR("hello world from ros");
        loop_rate.sleep();
    }

    return 0;
}