//
// Created by sunhao on 4/6/21.
//

#include <ros/ros.h>

#include <std_msgs/String.h>

int main(int argc, char** argv){
    ros::init(argc,argv,"talker");
    ros::NodeHandle nh;

    auto puber = nh.advertise<std_msgs::String>("/udp/sub",1);

    ros::Rate loop_rate(1);
    std_msgs::String msg_string;
    //float a =0 ;
    msg_string.data = "hello world from talker";
    while (ros::ok()){
        puber.publish(msg_string);
        loop_rate.sleep();
    }


    return 0;
}
