//
// Created by sunhao on 6/22/19.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <errno.h>

#include <ros/ros.h>
#include <std_msgs/String.h>
//#define IP "192.168.1.15"
//#define PORT 6666

int main(int argc,char** argv)
{
    ros::init(argc,argv,"udp_recv");
    ros::NodeHandle nh,nh_para("~");
    //-------------------------------
    std::string topic ;
    nh_para.param<std::string>("topic",topic, "/udp_pub");
    ROS_INFO("#From udp recv# udp_pub =%s",topic.c_str());
    ros::Publisher pub = nh.advertise<std_msgs::String>(topic,1);

    int recv_port ;
    nh_para.param<int>("recv_port",recv_port,6666);
    ROS_INFO("#From udp recv# recv_port = %d",recv_port);

    int buf_size;
    nh_para.param<int>("buf_size",buf_size,100);
    ROS_INFO("#From udp recv# buf_size = %d",buf_size);
    //------------------------------
    int cnt = -1;
    int sfd = -1;
    int len = -1;
    struct sockaddr_in ser_addr;
    struct sockaddr_in from_addr;
    char* buf = new char[buf_size];
    memset(buf,0,buf_size);

    char ser_buf[128] = {0};
    //socket
    sfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(0 > sfd)
    {
        perror("socket error:");
        return -1;
    }

    //bind
    {
        //init
        ser_addr.sin_family = AF_INET;
        ser_addr.sin_port = htons(recv_port);
        ser_addr.sin_addr.s_addr = INADDR_ANY;//inet_addr(IP);
    }
    cnt = bind(sfd, (struct sockaddr *)&ser_addr, sizeof(struct sockaddr));
    if(0 > cnt)
    {
        perror("bind error:");
        return -1;
    }
    //return 0;
    std_msgs::String msg;
    while(ros::ok())
    {
        //recvfrom
        msg.data.clear();
        len = sizeof(struct sockaddr);
        memset(&from_addr, 0, sizeof(struct sockaddr));
        //memset(buf, 0, sizeof(buf));
        cnt = recvfrom(sfd, buf, buf_size, 0, (struct sockaddr *)&from_addr, (socklen_t*)&len);
        if(0 > cnt)
        {
            perror("received error:");
            return -1;
        }
       // ROS_ERROR("recv_cnt=%d",cnt);
//        ROS_ERROR("buf=%s",buf);
        for(int i =0;(i<cnt) && (i<buf_size);++i)
        {
            msg.data += buf[i];
        }
        pub.publish(msg);
    }

    //close
    delete(buf);
    close(sfd);
    return 0;
}