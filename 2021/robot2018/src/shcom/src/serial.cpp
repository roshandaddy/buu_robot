//
// Created by sunhao on 7/6/19.
//



#include <ros/ros.h>
#include <serial/serial.h>
#include <std_msgs/String.h>

#include <string>


static std_msgs::String send_str;
void callbackSerilStr(const std_msgs::String::ConstPtr & msg)
{
    send_str = * msg;
}


int main(int argc,char ** argv)
{
    ros::init(argc,argv,"serial_com");
    ros::NodeHandle nh,nh_para("~");


    //----------init-----------------
    std::string  port;
    nh_para.param<std::string>("serial_port",port,"/dev/USBttyS0");
    ROS_ERROR("#From serial_com# Serial port = %s",port.c_str());

    int freq;
    nh_para.param<int>("freq",freq,10);
    ROS_ERROR("#From serial_com# freq= %d",freq);

    int baud_rate;
    nh_para.param<int>("baud_rate",baud_rate,115200);
    ROS_ERROR("#From serial_com# baud_rate= %d",baud_rate);

    std::string sub_tpk,pub_tpk;
    nh_para.param<std::string>("sub_tpk",sub_tpk,"/serial/sub");
    ROS_ERROR("#From serial_com# sub_tpk = %s",sub_tpk.c_str());

    nh_para.param<std::string>("pub_tpk",pub_tpk,"/serial/pub");
    ROS_ERROR("#From serial_com# pub_tpk = %s",pub_tpk.c_str());

    ros::Subscriber sub =  nh.subscribe(sub_tpk,1,&callbackSerilStr);
    ros::Publisher pub = nh.advertise<std_msgs::String>(pub_tpk,1);
    //------------------------
    serial::Serial ser;
    ros::Rate openLoopRate(1);//
    while( !ser.isOpen()  )
    {
        try
        {
            //设置串口属性，并打开串口
            ser.setPort(port);
            ser.setBaudrate(baud_rate);
            serial::Timeout to = serial::Timeout::simpleTimeout(5);//
            ser.setTimeout(to);
            ser.open();
        }
        catch (serial::IOException& e)
        {
            ROS_ERROR_STREAM("Unable to open port!!!");
//            return -1;
        }
        openLoopRate.sleep();
    }
    ROS_INFO_STREAM("Serial Port initialized!!!");

    //----------------------
    ros::Rate loop_rate(freq);
    while ( ros::ok() )
    {
        //---------serial recv data and pub---------------
        long unsigned int serial_buf_cnt = ser.available();
        if(serial_buf_cnt)
        {
            std_msgs::String pub_msg;
            pub_msg.data = ser.read(serial_buf_cnt);
            pub.publish(pub_msg);
        }

        //-------sub data and serial send data --------------
        ros::spinOnce();
        size_t bytes_wrote = ser.write(send_str.data);

        loop_rate.sleep();
    }


    return 0;
}