//
// Created by sunhao on 6/22/19.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <errno.h>

#include <ros/ros.h>
#include <std_msgs/String.h>

//#define IP "192.168.1.12"
//#define PORT 6666


static std_msgs::String g_msg;
void callbackUDP(const std_msgs::String::ConstPtr & msg)
{
    g_msg = *msg;
    //ROS_ERROR("callback size = %d",g_msg.data.size());
}


int main(int argc,char** argv)
{
    ros::init(argc,argv,"udp_send");
    ros::NodeHandle nh,nh_para("~");


    std::string topic = "/udp/sub";
    nh_para.param<std::string>("topic",topic,topic);
    ros::Subscriber sub = nh.subscribe(topic,1,callbackUDP);
    ROS_INFO("#From udp_send# sub_tpk= %s",topic.c_str());

    std::string send_ip = "127.0.0.1";
    nh_para.param("send_ip",send_ip,send_ip);
    ROS_INFO("#From udp_send# send_ip= %s",send_ip.c_str());

    int send_port ;
    nh_para.param("send_port",send_port,6666);
    ROS_INFO("#From udp_send# send_port= %d",send_port);

    int buf_size;
    nh_para.param("buf_size",buf_size,100);
    ROS_INFO("#From udp_send# buf_size= %d",buf_size);

    char ip[20]={0};
    int ip_len = send_ip.size();
    for( int i=0;i<ip_len;++i )
    {
        ip[i] = send_ip[i];
    }
    //-----------------------------------
    int cnt = -1;
    int sfd = -1;
    int len = -1;
    struct sockaddr_in ser_addr;
    char* buf = new char[buf_size];
    memset(buf,0,buf_size);


    //char client_buf[128] = {0};
    //socket
    sfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(0 > sfd)
    {
        ROS_ERROR("socket error:");
        return -1;
    }

    //init
    len = sizeof(struct sockaddr);
    ser_addr.sin_family = AF_INET;
    ser_addr.sin_port = htons(send_port);
    ser_addr.sin_addr.s_addr = inet_addr(ip);

    int freq;
    nh_para.param("frequence",freq,10);
    ROS_INFO("#From udp_send# freq= %d",freq);
    ros::Rate loop_rate(freq);
    while(ros::ok())
    {
        ros::spinOnce();
        if (g_msg.data.empty()){
            loop_rate.sleep();
            continue;
        }
        auto msg_size = g_msg.data.size();
        for(int i=0;i<msg_size;++i)
        {
            buf[i] = g_msg.data[i];
            if( i== (buf_size -1) )
            {
                msg_size = buf_size -1;
                ROS_ERROR("buffer is not long enough!");
                break;
            }
        }
        g_msg.data.clear();
        cnt = sendto(sfd, buf, msg_size, 0, (struct sockaddr *)&ser_addr, len);

        if(0 > cnt)
        {
            ROS_ERROR("sendto error: %d",cnt);
            return -1;
        }
        loop_rate.sleep();
    }

    //close
    delete(buf);
    close(sfd);
    return 0;
}