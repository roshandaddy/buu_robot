//
// Created by sunhao on 6/21/19.
//


#include <ros/ros.h>
#include <std_msgs/String.h>

#include "udp/sh_udp.h"


#define UDP_RECV        1001
#define UDP_SEND        1002
#define UDP_RECV_SEND   1003

static std_msgs::String g_msg;
void callbackUDP(const std_msgs::String::ConstPtr & msg)
{
    g_msg = *msg;
    //ROS_ERROR("callback");
}


int main(int argc, char** argv)
{
    ros::init(argc,argv,"udp_com");
    ros::NodeHandle nh,nh_para("~");


    int type;
    nh_para.param("type",type,1002);

    if( type!=UDP_RECV &&  type!=UDP_SEND && type!=UDP_RECV_SEND  )
    {
        printf("Please enter correct type!\n");
        return 0;
    }

    //----------------------------------
    std::string sub_topoic = "/udp_sub";
    std::string pub_topic = "/udp_pub";

    nh_para.param("sub_topic",sub_topoic,sub_topoic);
    nh_para.param("pub_topic",pub_topic,pub_topic);

    ros::Subscriber sub = nh.subscribe(sub_topoic,10,callbackUDP);
    ros::Publisher pub = nh.advertise<std_msgs::String>(pub_topic,1);

    //----------------------------------
//    std::string my_ip = "192.168.001.012";
//    nh_para.param("my_ip",my_ip,my_ip);

    std::string remote_ip = "192.168.001.012";
    nh_para.param("remote_ip",remote_ip,remote_ip);

    int my_port;
    nh_para.param("my_port",my_port,1011);
    int remote_port;
    nh_para.param("remote_port",remote_port,1010);


    int buf_size;
    nh_para.param("buf_size",buf_size,1000);
    //-----------------------------
    sh_udp::ComUDP com_udp;
    com_udp.setRemoteIP(remote_ip);
    com_udp.setRemotePort(remote_port);
    com_udp.setMyPort(my_port);
    com_udp.setBufSize(buf_size);
    //com_udp.setMyIP(my_ip);


    if (com_udp.init() )
    {
        return 0;
    }
    ROS_INFO("start working!");
    //------------------------
    int freq;
    nh_para.param("frequence",freq,1);
    ros::Rate loop_rate(freq);
    std_msgs::String recv_msg;
    std::string send_data;
    while (ros::ok())
    {
        ros::spinOnce();

        if( type == UDP_RECV )
        {
            com_udp.recv( recv_msg.data );
            pub.publish(recv_msg);
            ROS_ERROR("receving");
        }
        if( type == UDP_SEND )
        {
            send_data = g_msg.data;
            ROS_ERROR("sending");
            com_udp.send(send_data);
        }
        if( type == UDP_RECV_SEND )
        {
            //------recv ---
            com_udp.recv( recv_msg.data );
            pub.publish(recv_msg);
            //------send -----
            send_data = g_msg.data;
            com_udp.send(send_data);
        }
        loop_rate.sleep();
    }

    return 0;
}
