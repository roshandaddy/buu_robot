/*
*
*
*
*
*/



#include "sh_udp.h"


namespace sh_udp
{

ComUDP::ComUDP():remote_ip_flag_(false),remote_port_flag_(false),my_ip_flag_(false),my_port_flag_(false)
{

}
//-------------------
ComUDP::~ComUDP()
{
    delete(buf_);
    //delete(remote_ip_);
    //delete(my_ip_);
    close(com_socket_);
}
//-------------------
int ComUDP::init()
{
    //---------------------
    if( !remote_ip_flag_ )
    {
        return -1;
    }
    if(!remote_port_flag_)
    {
        return -1;
    }
//    if( !my_ip_flag_ )
//    {
//        return -1;
//    }
    if( !my_port_flag_ )
    {
        return -1;
    }

    com_socket_ = -1;
    //-------------------------------
    if((com_socket_=socket(AF_INET,SOCK_DGRAM,0))<0)
    {
        printf( " socket error" );
        return -1;
    }
    int len = sizeof(my_addr_);
    addr_len_ = sizeof(struct sockaddr_in );
    //-----------------------------
//#define IP  "192.168.011.112"
    memset(&remote_addr_, 0, len);
    remote_addr_.sin_family      = AF_INET; //IP地址类型为IPv4
    remote_addr_.sin_port        = htons(remote_port_); //端口号
    remote_addr_.sin_addr.s_addr = inet_addr(remote_ip_);//绑定IP para: const char *__cp

    //设置IP地址
    //-----------------------------

    memset(&my_addr_, 0, len);
    my_addr_.sin_family      = AF_INET; //IP地址类型为IPv4
    my_addr_.sin_port        = htons( my_port_); //端口号
    my_addr_.sin_addr.s_addr = INADDR_ANY;//绑定IP para: const char *__
    //my_addr_.sin_addr.s_addr = inet_addr(remote_ip_);//绑定IP para: const char *__cp
    // 第二步，bind服务器的IP
    if((bind(com_socket_,(const struct sockaddr *)&my_addr_,sizeof(my_addr_)))<0)
    {
        printf ("bind error\n");
        return -1;
    }
    else
    {
        printf("bind had success!\n");
        return 0;
    }
}
//-------------------
int ComUDP::recv(std::string & data)
{
    data.clear();
    recv_cnt_ = recvfrom(com_socket_, buf_, sizeof(buf_),0,(struct sockaddr *) &remote_addr_,(socklen_t *) & addr_len_);

    ROS_ERROR("recv_cnt = %d",recv_cnt_);
    for( int i=0;i<recv_cnt_;++i )
    {
        data.push_back( buf_[i] );
    }
    return 0;
}
//-------------------
int ComUDP::send(const std::string & data)
{
    for( int i=0;i<data.size();++i )
    {
        if( i >= sizeof(buf_)  ) //
        {
            break;
        }
        buf_[i] = data[i];

    }
    send_cnt_ = sendto(com_socket_, buf_, data.size(), 0, (struct sockaddr *)& remote_addr_,  sizeof(struct sockaddr));
    ROS_ERROR("sub cnt=%d,send cnt=%d",data.size(),send_cnt_);
    ROS_ERROR("errno = %d", errno);
    return 0;
}

void ComUDP::setRemoteIP(const std::string & ip)
{
    int len = ip.size();
    //remote_ip_ =  new char[len];
    memset(remote_ip_, 0, len);
    //memset(send_ip, 0, 30);
    for( int i=0;i<ip.size();++i )
    {
        remote_ip_[i] = ip[i];
    }
    //remote_ip_[len]='\0';
    remote_ip_flag_ = true;
}
//-------------------
void ComUDP::setRemotePort(const unsigned int & port)
{
    remote_port_ =  port;
    remote_port_flag_ =  true;
}
//-------------------
void ComUDP::setMyIP(const std::string & ip)
{
//    int len = ip.size();
//    my_ip_ =  new char[len];
//    memset(my_ip_, 0, len);
//    for( int i=0;i<len;++i )
//    {
//        my_ip_[i] = ip[i];
//    }
//    //my_ip_[len]='\0';
//    my_ip_flag_ = true;
}

//-------------------
void ComUDP::setMyPort(const unsigned int & port)
{
    my_port_ = port;
    my_port_flag_ = true;
}

}


