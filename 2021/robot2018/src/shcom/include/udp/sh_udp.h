/*
*  作者： 孙 浩
*  命名空间： sh_work
*  作用： udp 发送和接受函数
*/

#ifndef COM_SH_H
#define COM_SH_H



#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <thread>
#include <iostream>
#include <string.h>
#include <vector>
#include <errno.h>

#include <ros/ros.h>

namespace sh_udp
{

//--------------------------
class ComUDP
{
    public:
    ComUDP();
    ~ComUDP();

public:

    void setRemoteIP(const std::string & ip);
    void setRemotePort(const unsigned int &port);
    void setMyIP(const std::string & ip);
    void setMyPort(const unsigned int & port);
    void setBufSize(const unsigned int & size) {
        buf_ = new char[size];
    }


    int init();
    int recv(std::string & data);
    int send(const std::string & data);

private:
    //char *remote_ip_;
    char *my_ip_;
    char remote_ip_[30];
    unsigned int remote_port_,my_port_;
    int com_socket_ = -1;
    struct sockaddr_in remote_addr_,my_addr_;
    bool remote_ip_flag_,my_ip_flag_,remote_port_flag_,my_port_flag_;

    char* buf_;
    int recv_cnt_;
    int send_cnt_;

    int addr_len_;

};
}
#endif
