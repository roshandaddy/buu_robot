//
// Created by sunhao on 5/18/21.
//


#include <ros/ros.h>
//#include <ros/time.h>
#include <tf/tf.h>
# include <nav_msgs/Path.h>
# include <geometry_msgs/PoseStamped.h>

void getLine(nav_msgs::Path & line){
    line.header.frame_id = "carla";
    line.header.stamp = ros::Time::now();

    geometry_msgs::PoseStamped pose1,pose2;

    pose1.pose.position.x = 0;
    pose1.pose.position.y = 0;
    pose1.pose.position.z = 0;


    pose2.pose.position.x = 0;
    pose2.pose.position.y = 10;
    pose2.pose.position.z = 0;

    line.poses.push_back(pose1);
    line.poses.push_back(pose2);

}


//-----------------------------------------
int main(int argc, char** argv){
    ros::init(argc,argv,"viz_line");
    ros::NodeHandle  nh, nh_para("~");

    auto puber = nh.advertise<nav_msgs::Path>("/roshan/line",1);


    nav_msgs::Path  line;
    getLine(line);

    ros::Rate loop_rate(10);
    while (ros::ok()){
        puber.publish(line);
        loop_rate.sleep();
    }

    return 0;
}