//
// Created by sunhao on 5/18/21.
//

#include <ros/ros.h>
#include <tf/tf.h>
#include <visualization_msgs/Marker.h>


void getMarker(visualization_msgs::Marker & veh_marker_,
        const float & x,
        const float & y,
        const float & yaw){

    veh_marker_.header.stamp = ros::Time::now();
    veh_marker_.header.frame_id = "carla";

    veh_marker_.ns = "veh";
    veh_marker_.id = 2;

    veh_marker_.type = visualization_msgs::Marker::CUBE;
    veh_marker_.action = visualization_msgs::Marker::ADD;

    veh_marker_.scale.x = 3.0;
    veh_marker_.scale.y = 1.8;
    veh_marker_.scale.z = 1.0;

    veh_marker_.color.r = 1.0f;
    veh_marker_.color.g = 0.0f;
    veh_marker_.color.b = 0.0f;
    veh_marker_.color.a = 1;

    veh_marker_.pose.position.x = x;
    veh_marker_.pose.position.y = y;
    veh_marker_.pose.position.z = 0;

    tf::Quaternion quat;
    quat.setRPY(0,0,yaw);

    veh_marker_.pose.orientation.x = quat.getX();
    veh_marker_.pose.orientation.y = quat.getY();
    veh_marker_.pose.orientation.z = quat.getZ();
    veh_marker_.pose.orientation.w = quat.getW();

    veh_marker_.lifetime = ros::Duration();
}

//-----------------------------------------
int main(int argc, char** argv){
    ros::init(argc,argv,"viz_marker");
    ros::NodeHandle  nh, nh_para("~");

    auto puber = nh.advertise<visualization_msgs::Marker>("/roshan/marker",1);


    visualization_msgs::Marker  marker;
    getMarker(marker,0,0,0.5);

    ros::Rate loop_rate(10);
    while (ros::ok()){
        puber.publish(marker);
        loop_rate.sleep();
    }

    return 0;
}