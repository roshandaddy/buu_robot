// Auto-generated. Do not edit!

// (in-package hdmap_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let Point = require('./Point.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class HDMapPath {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.path = null;
      this.left_lane = null;
      this.right_lane = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('path')) {
        this.path = initObj.path
      }
      else {
        this.path = [];
      }
      if (initObj.hasOwnProperty('left_lane')) {
        this.left_lane = initObj.left_lane
      }
      else {
        this.left_lane = [];
      }
      if (initObj.hasOwnProperty('right_lane')) {
        this.right_lane = initObj.right_lane
      }
      else {
        this.right_lane = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type HDMapPath
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [path]
    // Serialize the length for message field [path]
    bufferOffset = _serializer.uint32(obj.path.length, buffer, bufferOffset);
    obj.path.forEach((val) => {
      bufferOffset = Point.serialize(val, buffer, bufferOffset);
    });
    // Serialize message field [left_lane]
    // Serialize the length for message field [left_lane]
    bufferOffset = _serializer.uint32(obj.left_lane.length, buffer, bufferOffset);
    obj.left_lane.forEach((val) => {
      bufferOffset = Point.serialize(val, buffer, bufferOffset);
    });
    // Serialize message field [right_lane]
    // Serialize the length for message field [right_lane]
    bufferOffset = _serializer.uint32(obj.right_lane.length, buffer, bufferOffset);
    obj.right_lane.forEach((val) => {
      bufferOffset = Point.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type HDMapPath
    let len;
    let data = new HDMapPath(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [path]
    // Deserialize array length for message field [path]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.path = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.path[i] = Point.deserialize(buffer, bufferOffset)
    }
    // Deserialize message field [left_lane]
    // Deserialize array length for message field [left_lane]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.left_lane = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.left_lane[i] = Point.deserialize(buffer, bufferOffset)
    }
    // Deserialize message field [right_lane]
    // Deserialize array length for message field [right_lane]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.right_lane = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.right_lane[i] = Point.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    object.path.forEach((val) => {
      length += Point.getMessageSize(val);
    });
    object.left_lane.forEach((val) => {
      length += Point.getMessageSize(val);
    });
    object.right_lane.forEach((val) => {
      length += Point.getMessageSize(val);
    });
    return length + 12;
  }

  static datatype() {
    // Returns string type for a message object
    return 'hdmap_msgs/HDMapPath';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'd4beb0be1c23307204027f62a58ce27f';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #############################
    ###       SunHao          ###
    #############################
    
    
    std_msgs/Header header
    
    hdmap_msgs/Point[] path
    hdmap_msgs/Point[] left_lane
    hdmap_msgs/Point[] right_lane
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: hdmap_msgs/Point
    #############################
    ###       SunHao          ###
    #############################
    
    float64 x
    float64 y
    float64 z
    float64 s
    float64 cuv
    float64 heading
    float64 speed
    float64 speedkmh
    
    ############################
    
    int32 speed_mode  # 速度模式["1:ultra_high", "2:high", "3:high_medium", "4:medium", "5:medium_low", "6:low", "7:ultra_low"]
    int32 obs_strategy  # 障碍物处理策略["1:follow", "2:left_chage_lane", "3:right_change_lane", "4:vfh", "5:blind"]
    int32 follow_strategy  # 循迹优先级["1:rtk-radar-video", "2:rtk-video-radar", "3:video-radar-rtk", "4:video-rtk-radar","5:radar-rtk-video", "6:radar-rtk-video"]
    int32 special_mode  # 特殊属性["1:normal", "2:merge_in", "3:backward", "4:short_wait", "5:longwait", "6:endpoint"]
    int32 obs_search_strategy  # 搜索障碍物策略["1:normal", "2:wide", "3:very_wide", "4:narrow", "5:very_narrow"]
    int32 cross_option  # 路口属性策略["1:not cross", "2:no trafficlight", "3:trafficlight"]
    int32 reserved_option  # 保留字段
    
    ############################
    
    #int32 id
    std_msgs/String uuid
    
    #############################
    
    #int32 line_id
    std_msgs/String lane_uuid
    
    #############################
    
    #int32 section_id
    std_msgs/String section_uuid
    
    ================================================================================
    MSG: std_msgs/String
    string data
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new HDMapPath(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.path !== undefined) {
      resolved.path = new Array(msg.path.length);
      for (let i = 0; i < resolved.path.length; ++i) {
        resolved.path[i] = Point.Resolve(msg.path[i]);
      }
    }
    else {
      resolved.path = []
    }

    if (msg.left_lane !== undefined) {
      resolved.left_lane = new Array(msg.left_lane.length);
      for (let i = 0; i < resolved.left_lane.length; ++i) {
        resolved.left_lane[i] = Point.Resolve(msg.left_lane[i]);
      }
    }
    else {
      resolved.left_lane = []
    }

    if (msg.right_lane !== undefined) {
      resolved.right_lane = new Array(msg.right_lane.length);
      for (let i = 0; i < resolved.right_lane.length; ++i) {
        resolved.right_lane[i] = Point.Resolve(msg.right_lane[i]);
      }
    }
    else {
      resolved.right_lane = []
    }

    return resolved;
    }
};

module.exports = HDMapPath;
