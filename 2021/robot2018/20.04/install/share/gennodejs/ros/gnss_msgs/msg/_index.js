
"use strict";

let GPRMC = require('./GPRMC.js');
let GPVTG = require('./GPVTG.js');
let GTIMU = require('./GTIMU.js');
let GPFPD = require('./GPFPD.js');
let PSAT = require('./PSAT.js');
let VehStat = require('./VehStat.js');
let GPGGA = require('./GPGGA.js');

module.exports = {
  GPRMC: GPRMC,
  GPVTG: GPVTG,
  GTIMU: GTIMU,
  GPFPD: GPFPD,
  PSAT: PSAT,
  VehStat: VehStat,
  GPGGA: GPGGA,
};
