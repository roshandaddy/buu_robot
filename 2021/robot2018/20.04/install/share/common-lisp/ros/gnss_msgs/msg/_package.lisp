(cl:defpackage gnss_msgs-msg
  (:use )
  (:export
   "<GPFPD>"
   "GPFPD"
   "<GPGGA>"
   "GPGGA"
   "<GPRMC>"
   "GPRMC"
   "<GPVTG>"
   "GPVTG"
   "<GTIMU>"
   "GTIMU"
   "<PSAT>"
   "PSAT"
   "<VEHSTAT>"
   "VEHSTAT"
  ))

