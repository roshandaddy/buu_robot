;; Auto-generated. Do not edit!


(when (boundp 'hdmap_msgs::Map)
  (if (not (find-package "HDMAP_MSGS"))
    (make-package "HDMAP_MSGS"))
  (shadow 'Map (find-package "HDMAP_MSGS")))
(unless (find-package "HDMAP_MSGS::MAP")
  (make-package "HDMAP_MSGS::MAP"))

(in-package "ROS")
;;//! \htmlinclude Map.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass hdmap_msgs::Map
  :super ros::object
  :slots (_sections _name _utm_origin_x _utm_origin_y _utm_origin_z ))

(defmethod hdmap_msgs::Map
  (:init
   (&key
    ((:sections __sections) ())
    ((:name __name) (instance std_msgs::String :init))
    ((:utm_origin_x __utm_origin_x) 0.0)
    ((:utm_origin_y __utm_origin_y) 0.0)
    ((:utm_origin_z __utm_origin_z) 0.0)
    )
   (send-super :init)
   (setq _sections __sections)
   (setq _name __name)
   (setq _utm_origin_x (float __utm_origin_x))
   (setq _utm_origin_y (float __utm_origin_y))
   (setq _utm_origin_z (float __utm_origin_z))
   self)
  (:sections
   (&rest __sections)
   (if (keywordp (car __sections))
       (send* _sections __sections)
     (progn
       (if __sections (setq _sections (car __sections)))
       _sections)))
  (:name
   (&rest __name)
   (if (keywordp (car __name))
       (send* _name __name)
     (progn
       (if __name (setq _name (car __name)))
       _name)))
  (:utm_origin_x
   (&optional __utm_origin_x)
   (if __utm_origin_x (setq _utm_origin_x __utm_origin_x)) _utm_origin_x)
  (:utm_origin_y
   (&optional __utm_origin_y)
   (if __utm_origin_y (setq _utm_origin_y __utm_origin_y)) _utm_origin_y)
  (:utm_origin_z
   (&optional __utm_origin_z)
   (if __utm_origin_z (setq _utm_origin_z __utm_origin_z)) _utm_origin_z)
  (:serialization-length
   ()
   (+
    ;; hdmap_msgs/Section[] _sections
    (apply #'+ (send-all _sections :serialization-length)) 4
    ;; std_msgs/String _name
    (send _name :serialization-length)
    ;; float64 _utm_origin_x
    8
    ;; float64 _utm_origin_y
    8
    ;; float64 _utm_origin_z
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; hdmap_msgs/Section[] _sections
     (write-long (length _sections) s)
     (dolist (elem _sections)
       (send elem :serialize s)
       )
     ;; std_msgs/String _name
       (send _name :serialize s)
     ;; float64 _utm_origin_x
       (sys::poke _utm_origin_x (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _utm_origin_y
       (sys::poke _utm_origin_y (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _utm_origin_z
       (sys::poke _utm_origin_z (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; hdmap_msgs/Section[] _sections
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _sections (let (r) (dotimes (i n) (push (instance hdmap_msgs::Section :init) r)) r))
     (dolist (elem- _sections)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; std_msgs/String _name
     (send _name :deserialize buf ptr-) (incf ptr- (send _name :serialization-length))
   ;; float64 _utm_origin_x
     (setq _utm_origin_x (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _utm_origin_y
     (setq _utm_origin_y (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _utm_origin_z
     (setq _utm_origin_z (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get hdmap_msgs::Map :md5sum-) "46b057bbed8964803a46c0c17fd16115")
(setf (get hdmap_msgs::Map :datatype-) "hdmap_msgs/Map")
(setf (get hdmap_msgs::Map :definition-)
      "#############################
###       SunHao          ###
#############################

hdmap_msgs/Section[] sections

#########################

std_msgs/String name

float64 utm_origin_x
float64 utm_origin_y
float64 utm_origin_z


================================================================================
MSG: hdmap_msgs/Section
#############################
###       SunHao          ###
#############################



hdmap_msgs/Lane[] lanes

int32 direction_mode

#############################
#int32 id
std_msgs/String uuid

#############################

std_msgs/String[] to_section_uuid
std_msgs/String[] from_section_uuid


std_msgs/String[] to_lane_uuid
std_msgs/String[] from_lane_uuid


std_msgs/String[] to_pt_uuid
std_msgs/String[] from_pt_uuid

#int32[] to_pt_id
#int32[] from_pt_id

#############################

float64 length
float64 highest_speed
float64 traffic_cost
================================================================================
MSG: hdmap_msgs/Lane
#############################
###       SunHao          ###
#############################

hdmap_msgs/Point[] pts

#############################
int8 is_main # Is the lane center lane of this section
float64 offset
float64 width

#############################

#int32 id
std_msgs/String uuid

#############################

std_msgs/String main_uuid

#int32 left_id
std_msgs/String left_uuid

#int32 right_id
std_msgs/String right_uuid


#############################

#int32 section_id
std_msgs/String section_uuid

#############################
================================================================================
MSG: hdmap_msgs/Point
#############################
###       SunHao          ###
#############################

float64 x
float64 y
float64 z
float64 s
float64 cuv
float64 heading
float64 speed
float64 speedkmh

############################

int32 speed_mode  # 速度模式[\"1:ultra_high\", \"2:high\", \"3:high_medium\", \"4:medium\", \"5:medium_low\", \"6:low\", \"7:ultra_low\"]
int32 obs_strategy  # 障碍物处理策略[\"1:follow\", \"2:left_chage_lane\", \"3:right_change_lane\", \"4:vfh\", \"5:blind\"]
int32 follow_strategy  # 循迹优先级[\"1:rtk-radar-video\", \"2:rtk-video-radar\", \"3:video-radar-rtk\", \"4:video-rtk-radar\",\"5:radar-rtk-video\", \"6:radar-rtk-video\"]
int32 special_mode  # 特殊属性[\"1:normal\", \"2:merge_in\", \"3:backward\", \"4:short_wait\", \"5:longwait\", \"6:endpoint\"]
int32 obs_search_strategy  # 搜索障碍物策略[\"1:normal\", \"2:wide\", \"3:very_wide\", \"4:narrow\", \"5:very_narrow\"]
int32 cross_option  # 路口属性策略[\"1:not cross\", \"2:no trafficlight\", \"3:trafficlight\"]
int32 reserved_option  # 保留字段

############################

#int32 id
std_msgs/String uuid

#############################

#int32 line_id
std_msgs/String lane_uuid

#############################

#int32 section_id
std_msgs/String section_uuid

================================================================================
MSG: std_msgs/String
string data

")



(provide :hdmap_msgs/Map "46b057bbed8964803a46c0c17fd16115")


