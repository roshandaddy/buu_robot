;; Auto-generated. Do not edit!


(when (boundp 'hdmap_msgs::Point)
  (if (not (find-package "HDMAP_MSGS"))
    (make-package "HDMAP_MSGS"))
  (shadow 'Point (find-package "HDMAP_MSGS")))
(unless (find-package "HDMAP_MSGS::POINT")
  (make-package "HDMAP_MSGS::POINT"))

(in-package "ROS")
;;//! \htmlinclude Point.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass hdmap_msgs::Point
  :super ros::object
  :slots (_x _y _z _s _cuv _heading _speed _speedkmh _speed_mode _obs_strategy _follow_strategy _special_mode _obs_search_strategy _cross_option _reserved_option _uuid _lane_uuid _section_uuid ))

(defmethod hdmap_msgs::Point
  (:init
   (&key
    ((:x __x) 0.0)
    ((:y __y) 0.0)
    ((:z __z) 0.0)
    ((:s __s) 0.0)
    ((:cuv __cuv) 0.0)
    ((:heading __heading) 0.0)
    ((:speed __speed) 0.0)
    ((:speedkmh __speedkmh) 0.0)
    ((:speed_mode __speed_mode) 0)
    ((:obs_strategy __obs_strategy) 0)
    ((:follow_strategy __follow_strategy) 0)
    ((:special_mode __special_mode) 0)
    ((:obs_search_strategy __obs_search_strategy) 0)
    ((:cross_option __cross_option) 0)
    ((:reserved_option __reserved_option) 0)
    ((:uuid __uuid) (instance std_msgs::String :init))
    ((:lane_uuid __lane_uuid) (instance std_msgs::String :init))
    ((:section_uuid __section_uuid) (instance std_msgs::String :init))
    )
   (send-super :init)
   (setq _x (float __x))
   (setq _y (float __y))
   (setq _z (float __z))
   (setq _s (float __s))
   (setq _cuv (float __cuv))
   (setq _heading (float __heading))
   (setq _speed (float __speed))
   (setq _speedkmh (float __speedkmh))
   (setq _speed_mode (round __speed_mode))
   (setq _obs_strategy (round __obs_strategy))
   (setq _follow_strategy (round __follow_strategy))
   (setq _special_mode (round __special_mode))
   (setq _obs_search_strategy (round __obs_search_strategy))
   (setq _cross_option (round __cross_option))
   (setq _reserved_option (round __reserved_option))
   (setq _uuid __uuid)
   (setq _lane_uuid __lane_uuid)
   (setq _section_uuid __section_uuid)
   self)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:z
   (&optional __z)
   (if __z (setq _z __z)) _z)
  (:s
   (&optional __s)
   (if __s (setq _s __s)) _s)
  (:cuv
   (&optional __cuv)
   (if __cuv (setq _cuv __cuv)) _cuv)
  (:heading
   (&optional __heading)
   (if __heading (setq _heading __heading)) _heading)
  (:speed
   (&optional __speed)
   (if __speed (setq _speed __speed)) _speed)
  (:speedkmh
   (&optional __speedkmh)
   (if __speedkmh (setq _speedkmh __speedkmh)) _speedkmh)
  (:speed_mode
   (&optional __speed_mode)
   (if __speed_mode (setq _speed_mode __speed_mode)) _speed_mode)
  (:obs_strategy
   (&optional __obs_strategy)
   (if __obs_strategy (setq _obs_strategy __obs_strategy)) _obs_strategy)
  (:follow_strategy
   (&optional __follow_strategy)
   (if __follow_strategy (setq _follow_strategy __follow_strategy)) _follow_strategy)
  (:special_mode
   (&optional __special_mode)
   (if __special_mode (setq _special_mode __special_mode)) _special_mode)
  (:obs_search_strategy
   (&optional __obs_search_strategy)
   (if __obs_search_strategy (setq _obs_search_strategy __obs_search_strategy)) _obs_search_strategy)
  (:cross_option
   (&optional __cross_option)
   (if __cross_option (setq _cross_option __cross_option)) _cross_option)
  (:reserved_option
   (&optional __reserved_option)
   (if __reserved_option (setq _reserved_option __reserved_option)) _reserved_option)
  (:uuid
   (&rest __uuid)
   (if (keywordp (car __uuid))
       (send* _uuid __uuid)
     (progn
       (if __uuid (setq _uuid (car __uuid)))
       _uuid)))
  (:lane_uuid
   (&rest __lane_uuid)
   (if (keywordp (car __lane_uuid))
       (send* _lane_uuid __lane_uuid)
     (progn
       (if __lane_uuid (setq _lane_uuid (car __lane_uuid)))
       _lane_uuid)))
  (:section_uuid
   (&rest __section_uuid)
   (if (keywordp (car __section_uuid))
       (send* _section_uuid __section_uuid)
     (progn
       (if __section_uuid (setq _section_uuid (car __section_uuid)))
       _section_uuid)))
  (:serialization-length
   ()
   (+
    ;; float64 _x
    8
    ;; float64 _y
    8
    ;; float64 _z
    8
    ;; float64 _s
    8
    ;; float64 _cuv
    8
    ;; float64 _heading
    8
    ;; float64 _speed
    8
    ;; float64 _speedkmh
    8
    ;; int32 _speed_mode
    4
    ;; int32 _obs_strategy
    4
    ;; int32 _follow_strategy
    4
    ;; int32 _special_mode
    4
    ;; int32 _obs_search_strategy
    4
    ;; int32 _cross_option
    4
    ;; int32 _reserved_option
    4
    ;; std_msgs/String _uuid
    (send _uuid :serialization-length)
    ;; std_msgs/String _lane_uuid
    (send _lane_uuid :serialization-length)
    ;; std_msgs/String _section_uuid
    (send _section_uuid :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _x
       (sys::poke _x (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y
       (sys::poke _y (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _z
       (sys::poke _z (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _s
       (sys::poke _s (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _cuv
       (sys::poke _cuv (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _heading
       (sys::poke _heading (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _speed
       (sys::poke _speed (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _speedkmh
       (sys::poke _speedkmh (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; int32 _speed_mode
       (write-long _speed_mode s)
     ;; int32 _obs_strategy
       (write-long _obs_strategy s)
     ;; int32 _follow_strategy
       (write-long _follow_strategy s)
     ;; int32 _special_mode
       (write-long _special_mode s)
     ;; int32 _obs_search_strategy
       (write-long _obs_search_strategy s)
     ;; int32 _cross_option
       (write-long _cross_option s)
     ;; int32 _reserved_option
       (write-long _reserved_option s)
     ;; std_msgs/String _uuid
       (send _uuid :serialize s)
     ;; std_msgs/String _lane_uuid
       (send _lane_uuid :serialize s)
     ;; std_msgs/String _section_uuid
       (send _section_uuid :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _x
     (setq _x (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y
     (setq _y (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _z
     (setq _z (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _s
     (setq _s (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _cuv
     (setq _cuv (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _heading
     (setq _heading (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _speed
     (setq _speed (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _speedkmh
     (setq _speedkmh (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; int32 _speed_mode
     (setq _speed_mode (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _obs_strategy
     (setq _obs_strategy (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _follow_strategy
     (setq _follow_strategy (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _special_mode
     (setq _special_mode (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _obs_search_strategy
     (setq _obs_search_strategy (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _cross_option
     (setq _cross_option (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _reserved_option
     (setq _reserved_option (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; std_msgs/String _uuid
     (send _uuid :deserialize buf ptr-) (incf ptr- (send _uuid :serialization-length))
   ;; std_msgs/String _lane_uuid
     (send _lane_uuid :deserialize buf ptr-) (incf ptr- (send _lane_uuid :serialization-length))
   ;; std_msgs/String _section_uuid
     (send _section_uuid :deserialize buf ptr-) (incf ptr- (send _section_uuid :serialization-length))
   ;;
   self)
  )

(setf (get hdmap_msgs::Point :md5sum-) "a0b93940b099f366d7568fda23d39791")
(setf (get hdmap_msgs::Point :datatype-) "hdmap_msgs/Point")
(setf (get hdmap_msgs::Point :definition-)
      "#############################
###       SunHao          ###
#############################

float64 x
float64 y
float64 z
float64 s
float64 cuv
float64 heading
float64 speed
float64 speedkmh

############################

int32 speed_mode  # 速度模式[\"1:ultra_high\", \"2:high\", \"3:high_medium\", \"4:medium\", \"5:medium_low\", \"6:low\", \"7:ultra_low\"]
int32 obs_strategy  # 障碍物处理策略[\"1:follow\", \"2:left_chage_lane\", \"3:right_change_lane\", \"4:vfh\", \"5:blind\"]
int32 follow_strategy  # 循迹优先级[\"1:rtk-radar-video\", \"2:rtk-video-radar\", \"3:video-radar-rtk\", \"4:video-rtk-radar\",\"5:radar-rtk-video\", \"6:radar-rtk-video\"]
int32 special_mode  # 特殊属性[\"1:normal\", \"2:merge_in\", \"3:backward\", \"4:short_wait\", \"5:longwait\", \"6:endpoint\"]
int32 obs_search_strategy  # 搜索障碍物策略[\"1:normal\", \"2:wide\", \"3:very_wide\", \"4:narrow\", \"5:very_narrow\"]
int32 cross_option  # 路口属性策略[\"1:not cross\", \"2:no trafficlight\", \"3:trafficlight\"]
int32 reserved_option  # 保留字段

############################

#int32 id
std_msgs/String uuid

#############################

#int32 line_id
std_msgs/String lane_uuid

#############################

#int32 section_id
std_msgs/String section_uuid

================================================================================
MSG: std_msgs/String
string data

")



(provide :hdmap_msgs/Point "a0b93940b099f366d7568fda23d39791")


