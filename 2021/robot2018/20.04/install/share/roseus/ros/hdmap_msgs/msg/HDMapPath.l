;; Auto-generated. Do not edit!


(when (boundp 'hdmap_msgs::HDMapPath)
  (if (not (find-package "HDMAP_MSGS"))
    (make-package "HDMAP_MSGS"))
  (shadow 'HDMapPath (find-package "HDMAP_MSGS")))
(unless (find-package "HDMAP_MSGS::HDMAPPATH")
  (make-package "HDMAP_MSGS::HDMAPPATH"))

(in-package "ROS")
;;//! \htmlinclude HDMapPath.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass hdmap_msgs::HDMapPath
  :super ros::object
  :slots (_header _path _left_lane _right_lane ))

(defmethod hdmap_msgs::HDMapPath
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:path __path) ())
    ((:left_lane __left_lane) ())
    ((:right_lane __right_lane) ())
    )
   (send-super :init)
   (setq _header __header)
   (setq _path __path)
   (setq _left_lane __left_lane)
   (setq _right_lane __right_lane)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:path
   (&rest __path)
   (if (keywordp (car __path))
       (send* _path __path)
     (progn
       (if __path (setq _path (car __path)))
       _path)))
  (:left_lane
   (&rest __left_lane)
   (if (keywordp (car __left_lane))
       (send* _left_lane __left_lane)
     (progn
       (if __left_lane (setq _left_lane (car __left_lane)))
       _left_lane)))
  (:right_lane
   (&rest __right_lane)
   (if (keywordp (car __right_lane))
       (send* _right_lane __right_lane)
     (progn
       (if __right_lane (setq _right_lane (car __right_lane)))
       _right_lane)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; hdmap_msgs/Point[] _path
    (apply #'+ (send-all _path :serialization-length)) 4
    ;; hdmap_msgs/Point[] _left_lane
    (apply #'+ (send-all _left_lane :serialization-length)) 4
    ;; hdmap_msgs/Point[] _right_lane
    (apply #'+ (send-all _right_lane :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; hdmap_msgs/Point[] _path
     (write-long (length _path) s)
     (dolist (elem _path)
       (send elem :serialize s)
       )
     ;; hdmap_msgs/Point[] _left_lane
     (write-long (length _left_lane) s)
     (dolist (elem _left_lane)
       (send elem :serialize s)
       )
     ;; hdmap_msgs/Point[] _right_lane
     (write-long (length _right_lane) s)
     (dolist (elem _right_lane)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; hdmap_msgs/Point[] _path
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _path (let (r) (dotimes (i n) (push (instance hdmap_msgs::Point :init) r)) r))
     (dolist (elem- _path)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; hdmap_msgs/Point[] _left_lane
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _left_lane (let (r) (dotimes (i n) (push (instance hdmap_msgs::Point :init) r)) r))
     (dolist (elem- _left_lane)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; hdmap_msgs/Point[] _right_lane
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _right_lane (let (r) (dotimes (i n) (push (instance hdmap_msgs::Point :init) r)) r))
     (dolist (elem- _right_lane)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get hdmap_msgs::HDMapPath :md5sum-) "d4beb0be1c23307204027f62a58ce27f")
(setf (get hdmap_msgs::HDMapPath :datatype-) "hdmap_msgs/HDMapPath")
(setf (get hdmap_msgs::HDMapPath :definition-)
      "#############################
###       SunHao          ###
#############################


std_msgs/Header header

hdmap_msgs/Point[] path
hdmap_msgs/Point[] left_lane
hdmap_msgs/Point[] right_lane

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: hdmap_msgs/Point
#############################
###       SunHao          ###
#############################

float64 x
float64 y
float64 z
float64 s
float64 cuv
float64 heading
float64 speed
float64 speedkmh

############################

int32 speed_mode  # 速度模式[\"1:ultra_high\", \"2:high\", \"3:high_medium\", \"4:medium\", \"5:medium_low\", \"6:low\", \"7:ultra_low\"]
int32 obs_strategy  # 障碍物处理策略[\"1:follow\", \"2:left_chage_lane\", \"3:right_change_lane\", \"4:vfh\", \"5:blind\"]
int32 follow_strategy  # 循迹优先级[\"1:rtk-radar-video\", \"2:rtk-video-radar\", \"3:video-radar-rtk\", \"4:video-rtk-radar\",\"5:radar-rtk-video\", \"6:radar-rtk-video\"]
int32 special_mode  # 特殊属性[\"1:normal\", \"2:merge_in\", \"3:backward\", \"4:short_wait\", \"5:longwait\", \"6:endpoint\"]
int32 obs_search_strategy  # 搜索障碍物策略[\"1:normal\", \"2:wide\", \"3:very_wide\", \"4:narrow\", \"5:very_narrow\"]
int32 cross_option  # 路口属性策略[\"1:not cross\", \"2:no trafficlight\", \"3:trafficlight\"]
int32 reserved_option  # 保留字段

############################

#int32 id
std_msgs/String uuid

#############################

#int32 line_id
std_msgs/String lane_uuid

#############################

#int32 section_id
std_msgs/String section_uuid

================================================================================
MSG: std_msgs/String
string data

")



(provide :hdmap_msgs/HDMapPath "d4beb0be1c23307204027f62a58ce27f")


