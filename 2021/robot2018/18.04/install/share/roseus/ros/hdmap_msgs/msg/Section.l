;; Auto-generated. Do not edit!


(when (boundp 'hdmap_msgs::Section)
  (if (not (find-package "HDMAP_MSGS"))
    (make-package "HDMAP_MSGS"))
  (shadow 'Section (find-package "HDMAP_MSGS")))
(unless (find-package "HDMAP_MSGS::SECTION")
  (make-package "HDMAP_MSGS::SECTION"))

(in-package "ROS")
;;//! \htmlinclude Section.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass hdmap_msgs::Section
  :super ros::object
  :slots (_lanes _direction_mode _uuid _to_section_uuid _from_section_uuid _to_lane_uuid _from_lane_uuid _to_pt_uuid _from_pt_uuid _length _highest_speed _traffic_cost ))

(defmethod hdmap_msgs::Section
  (:init
   (&key
    ((:lanes __lanes) (let (r) (dotimes (i 0) (push (instance hdmap_msgs::Lane :init) r)) r))
    ((:direction_mode __direction_mode) 0)
    ((:uuid __uuid) (instance std_msgs::String :init))
    ((:to_section_uuid __to_section_uuid) (let (r) (dotimes (i 0) (push (instance std_msgs::String :init) r)) r))
    ((:from_section_uuid __from_section_uuid) (let (r) (dotimes (i 0) (push (instance std_msgs::String :init) r)) r))
    ((:to_lane_uuid __to_lane_uuid) (let (r) (dotimes (i 0) (push (instance std_msgs::String :init) r)) r))
    ((:from_lane_uuid __from_lane_uuid) (let (r) (dotimes (i 0) (push (instance std_msgs::String :init) r)) r))
    ((:to_pt_uuid __to_pt_uuid) (let (r) (dotimes (i 0) (push (instance std_msgs::String :init) r)) r))
    ((:from_pt_uuid __from_pt_uuid) (let (r) (dotimes (i 0) (push (instance std_msgs::String :init) r)) r))
    ((:length __length) 0.0)
    ((:highest_speed __highest_speed) 0.0)
    ((:traffic_cost __traffic_cost) 0.0)
    )
   (send-super :init)
   (setq _lanes __lanes)
   (setq _direction_mode (round __direction_mode))
   (setq _uuid __uuid)
   (setq _to_section_uuid __to_section_uuid)
   (setq _from_section_uuid __from_section_uuid)
   (setq _to_lane_uuid __to_lane_uuid)
   (setq _from_lane_uuid __from_lane_uuid)
   (setq _to_pt_uuid __to_pt_uuid)
   (setq _from_pt_uuid __from_pt_uuid)
   (setq _length (float __length))
   (setq _highest_speed (float __highest_speed))
   (setq _traffic_cost (float __traffic_cost))
   self)
  (:lanes
   (&rest __lanes)
   (if (keywordp (car __lanes))
       (send* _lanes __lanes)
     (progn
       (if __lanes (setq _lanes (car __lanes)))
       _lanes)))
  (:direction_mode
   (&optional __direction_mode)
   (if __direction_mode (setq _direction_mode __direction_mode)) _direction_mode)
  (:uuid
   (&rest __uuid)
   (if (keywordp (car __uuid))
       (send* _uuid __uuid)
     (progn
       (if __uuid (setq _uuid (car __uuid)))
       _uuid)))
  (:to_section_uuid
   (&rest __to_section_uuid)
   (if (keywordp (car __to_section_uuid))
       (send* _to_section_uuid __to_section_uuid)
     (progn
       (if __to_section_uuid (setq _to_section_uuid (car __to_section_uuid)))
       _to_section_uuid)))
  (:from_section_uuid
   (&rest __from_section_uuid)
   (if (keywordp (car __from_section_uuid))
       (send* _from_section_uuid __from_section_uuid)
     (progn
       (if __from_section_uuid (setq _from_section_uuid (car __from_section_uuid)))
       _from_section_uuid)))
  (:to_lane_uuid
   (&rest __to_lane_uuid)
   (if (keywordp (car __to_lane_uuid))
       (send* _to_lane_uuid __to_lane_uuid)
     (progn
       (if __to_lane_uuid (setq _to_lane_uuid (car __to_lane_uuid)))
       _to_lane_uuid)))
  (:from_lane_uuid
   (&rest __from_lane_uuid)
   (if (keywordp (car __from_lane_uuid))
       (send* _from_lane_uuid __from_lane_uuid)
     (progn
       (if __from_lane_uuid (setq _from_lane_uuid (car __from_lane_uuid)))
       _from_lane_uuid)))
  (:to_pt_uuid
   (&rest __to_pt_uuid)
   (if (keywordp (car __to_pt_uuid))
       (send* _to_pt_uuid __to_pt_uuid)
     (progn
       (if __to_pt_uuid (setq _to_pt_uuid (car __to_pt_uuid)))
       _to_pt_uuid)))
  (:from_pt_uuid
   (&rest __from_pt_uuid)
   (if (keywordp (car __from_pt_uuid))
       (send* _from_pt_uuid __from_pt_uuid)
     (progn
       (if __from_pt_uuid (setq _from_pt_uuid (car __from_pt_uuid)))
       _from_pt_uuid)))
  (:length
   (&optional __length)
   (if __length (setq _length __length)) _length)
  (:highest_speed
   (&optional __highest_speed)
   (if __highest_speed (setq _highest_speed __highest_speed)) _highest_speed)
  (:traffic_cost
   (&optional __traffic_cost)
   (if __traffic_cost (setq _traffic_cost __traffic_cost)) _traffic_cost)
  (:serialization-length
   ()
   (+
    ;; hdmap_msgs/Lane[] _lanes
    (apply #'+ (send-all _lanes :serialization-length)) 4
    ;; int32 _direction_mode
    4
    ;; std_msgs/String _uuid
    (send _uuid :serialization-length)
    ;; std_msgs/String[] _to_section_uuid
    (apply #'+ (send-all _to_section_uuid :serialization-length)) 4
    ;; std_msgs/String[] _from_section_uuid
    (apply #'+ (send-all _from_section_uuid :serialization-length)) 4
    ;; std_msgs/String[] _to_lane_uuid
    (apply #'+ (send-all _to_lane_uuid :serialization-length)) 4
    ;; std_msgs/String[] _from_lane_uuid
    (apply #'+ (send-all _from_lane_uuid :serialization-length)) 4
    ;; std_msgs/String[] _to_pt_uuid
    (apply #'+ (send-all _to_pt_uuid :serialization-length)) 4
    ;; std_msgs/String[] _from_pt_uuid
    (apply #'+ (send-all _from_pt_uuid :serialization-length)) 4
    ;; float64 _length
    8
    ;; float64 _highest_speed
    8
    ;; float64 _traffic_cost
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; hdmap_msgs/Lane[] _lanes
     (write-long (length _lanes) s)
     (dolist (elem _lanes)
       (send elem :serialize s)
       )
     ;; int32 _direction_mode
       (write-long _direction_mode s)
     ;; std_msgs/String _uuid
       (send _uuid :serialize s)
     ;; std_msgs/String[] _to_section_uuid
     (write-long (length _to_section_uuid) s)
     (dolist (elem _to_section_uuid)
       (send elem :serialize s)
       )
     ;; std_msgs/String[] _from_section_uuid
     (write-long (length _from_section_uuid) s)
     (dolist (elem _from_section_uuid)
       (send elem :serialize s)
       )
     ;; std_msgs/String[] _to_lane_uuid
     (write-long (length _to_lane_uuid) s)
     (dolist (elem _to_lane_uuid)
       (send elem :serialize s)
       )
     ;; std_msgs/String[] _from_lane_uuid
     (write-long (length _from_lane_uuid) s)
     (dolist (elem _from_lane_uuid)
       (send elem :serialize s)
       )
     ;; std_msgs/String[] _to_pt_uuid
     (write-long (length _to_pt_uuid) s)
     (dolist (elem _to_pt_uuid)
       (send elem :serialize s)
       )
     ;; std_msgs/String[] _from_pt_uuid
     (write-long (length _from_pt_uuid) s)
     (dolist (elem _from_pt_uuid)
       (send elem :serialize s)
       )
     ;; float64 _length
       (sys::poke _length (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _highest_speed
       (sys::poke _highest_speed (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _traffic_cost
       (sys::poke _traffic_cost (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; hdmap_msgs/Lane[] _lanes
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _lanes (let (r) (dotimes (i n) (push (instance hdmap_msgs::Lane :init) r)) r))
     (dolist (elem- _lanes)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; int32 _direction_mode
     (setq _direction_mode (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; std_msgs/String _uuid
     (send _uuid :deserialize buf ptr-) (incf ptr- (send _uuid :serialization-length))
   ;; std_msgs/String[] _to_section_uuid
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _to_section_uuid (let (r) (dotimes (i n) (push (instance std_msgs::String :init) r)) r))
     (dolist (elem- _to_section_uuid)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; std_msgs/String[] _from_section_uuid
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _from_section_uuid (let (r) (dotimes (i n) (push (instance std_msgs::String :init) r)) r))
     (dolist (elem- _from_section_uuid)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; std_msgs/String[] _to_lane_uuid
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _to_lane_uuid (let (r) (dotimes (i n) (push (instance std_msgs::String :init) r)) r))
     (dolist (elem- _to_lane_uuid)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; std_msgs/String[] _from_lane_uuid
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _from_lane_uuid (let (r) (dotimes (i n) (push (instance std_msgs::String :init) r)) r))
     (dolist (elem- _from_lane_uuid)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; std_msgs/String[] _to_pt_uuid
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _to_pt_uuid (let (r) (dotimes (i n) (push (instance std_msgs::String :init) r)) r))
     (dolist (elem- _to_pt_uuid)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; std_msgs/String[] _from_pt_uuid
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _from_pt_uuid (let (r) (dotimes (i n) (push (instance std_msgs::String :init) r)) r))
     (dolist (elem- _from_pt_uuid)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; float64 _length
     (setq _length (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _highest_speed
     (setq _highest_speed (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _traffic_cost
     (setq _traffic_cost (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get hdmap_msgs::Section :md5sum-) "3b7e8134e32592bc8f5f6e407594171e")
(setf (get hdmap_msgs::Section :datatype-) "hdmap_msgs/Section")
(setf (get hdmap_msgs::Section :definition-)
      "#############################
###       SunHao          ###
#############################



hdmap_msgs/Lane[] lanes

int32 direction_mode

#############################
#int32 id
std_msgs/String uuid

#############################

std_msgs/String[] to_section_uuid
std_msgs/String[] from_section_uuid


std_msgs/String[] to_lane_uuid
std_msgs/String[] from_lane_uuid


std_msgs/String[] to_pt_uuid
std_msgs/String[] from_pt_uuid

#int32[] to_pt_id
#int32[] from_pt_id

#############################

float64 length
float64 highest_speed
float64 traffic_cost
================================================================================
MSG: hdmap_msgs/Lane
#############################
###       SunHao          ###
#############################

hdmap_msgs/Point[] pts

#############################
int8 is_main # Is the lane center lane of this section
float64 offset
float64 width

#############################

#int32 id
std_msgs/String uuid

#############################

std_msgs/String main_uuid

#int32 left_id
std_msgs/String left_uuid

#int32 right_id
std_msgs/String right_uuid


#############################

#int32 section_id
std_msgs/String section_uuid

#############################
================================================================================
MSG: hdmap_msgs/Point
#############################
###       SunHao          ###
#############################

float64 x
float64 y
float64 z
float64 s
float64 cuv
float64 heading
float64 speed
float64 speedkmh

############################

int32 speed_mode  # 速度模式[\"1:ultra_high\", \"2:high\", \"3:high_medium\", \"4:medium\", \"5:medium_low\", \"6:low\", \"7:ultra_low\"]
int32 obs_strategy  # 障碍物处理策略[\"1:follow\", \"2:left_chage_lane\", \"3:right_change_lane\", \"4:vfh\", \"5:blind\"]
int32 follow_strategy  # 循迹优先级[\"1:rtk-radar-video\", \"2:rtk-video-radar\", \"3:video-radar-rtk\", \"4:video-rtk-radar\",\"5:radar-rtk-video\", \"6:radar-rtk-video\"]
int32 special_mode  # 特殊属性[\"1:normal\", \"2:merge_in\", \"3:backward\", \"4:short_wait\", \"5:longwait\", \"6:endpoint\"]
int32 obs_search_strategy  # 搜索障碍物策略[\"1:normal\", \"2:wide\", \"3:very_wide\", \"4:narrow\", \"5:very_narrow\"]
int32 cross_option  # 路口属性策略[\"1:not cross\", \"2:no trafficlight\", \"3:trafficlight\"]
int32 reserved_option  # 保留字段

############################

#int32 id
std_msgs/String uuid

#############################

#int32 line_id
std_msgs/String lane_uuid

#############################

#int32 section_id
std_msgs/String section_uuid

================================================================================
MSG: std_msgs/String
string data

")



(provide :hdmap_msgs/Section "3b7e8134e32592bc8f5f6e407594171e")


