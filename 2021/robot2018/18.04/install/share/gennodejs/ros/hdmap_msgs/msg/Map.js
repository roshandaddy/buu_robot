// Auto-generated. Do not edit!

// (in-package hdmap_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let Section = require('./Section.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class Map {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.sections = null;
      this.name = null;
      this.utm_origin_x = null;
      this.utm_origin_y = null;
      this.utm_origin_z = null;
    }
    else {
      if (initObj.hasOwnProperty('sections')) {
        this.sections = initObj.sections
      }
      else {
        this.sections = [];
      }
      if (initObj.hasOwnProperty('name')) {
        this.name = initObj.name
      }
      else {
        this.name = new std_msgs.msg.String();
      }
      if (initObj.hasOwnProperty('utm_origin_x')) {
        this.utm_origin_x = initObj.utm_origin_x
      }
      else {
        this.utm_origin_x = 0.0;
      }
      if (initObj.hasOwnProperty('utm_origin_y')) {
        this.utm_origin_y = initObj.utm_origin_y
      }
      else {
        this.utm_origin_y = 0.0;
      }
      if (initObj.hasOwnProperty('utm_origin_z')) {
        this.utm_origin_z = initObj.utm_origin_z
      }
      else {
        this.utm_origin_z = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Map
    // Serialize message field [sections]
    // Serialize the length for message field [sections]
    bufferOffset = _serializer.uint32(obj.sections.length, buffer, bufferOffset);
    obj.sections.forEach((val) => {
      bufferOffset = Section.serialize(val, buffer, bufferOffset);
    });
    // Serialize message field [name]
    bufferOffset = std_msgs.msg.String.serialize(obj.name, buffer, bufferOffset);
    // Serialize message field [utm_origin_x]
    bufferOffset = _serializer.float64(obj.utm_origin_x, buffer, bufferOffset);
    // Serialize message field [utm_origin_y]
    bufferOffset = _serializer.float64(obj.utm_origin_y, buffer, bufferOffset);
    // Serialize message field [utm_origin_z]
    bufferOffset = _serializer.float64(obj.utm_origin_z, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Map
    let len;
    let data = new Map(null);
    // Deserialize message field [sections]
    // Deserialize array length for message field [sections]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.sections = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.sections[i] = Section.deserialize(buffer, bufferOffset)
    }
    // Deserialize message field [name]
    data.name = std_msgs.msg.String.deserialize(buffer, bufferOffset);
    // Deserialize message field [utm_origin_x]
    data.utm_origin_x = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [utm_origin_y]
    data.utm_origin_y = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [utm_origin_z]
    data.utm_origin_z = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    object.sections.forEach((val) => {
      length += Section.getMessageSize(val);
    });
    length += std_msgs.msg.String.getMessageSize(object.name);
    return length + 28;
  }

  static datatype() {
    // Returns string type for a message object
    return 'hdmap_msgs/Map';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '46b057bbed8964803a46c0c17fd16115';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #############################
    ###       SunHao          ###
    #############################
    
    hdmap_msgs/Section[] sections
    
    #########################
    
    std_msgs/String name
    
    float64 utm_origin_x
    float64 utm_origin_y
    float64 utm_origin_z
    
    
    ================================================================================
    MSG: hdmap_msgs/Section
    #############################
    ###       SunHao          ###
    #############################
    
    
    
    hdmap_msgs/Lane[] lanes
    
    int32 direction_mode
    
    #############################
    #int32 id
    std_msgs/String uuid
    
    #############################
    
    std_msgs/String[] to_section_uuid
    std_msgs/String[] from_section_uuid
    
    
    std_msgs/String[] to_lane_uuid
    std_msgs/String[] from_lane_uuid
    
    
    std_msgs/String[] to_pt_uuid
    std_msgs/String[] from_pt_uuid
    
    #int32[] to_pt_id
    #int32[] from_pt_id
    
    #############################
    
    float64 length
    float64 highest_speed
    float64 traffic_cost
    ================================================================================
    MSG: hdmap_msgs/Lane
    #############################
    ###       SunHao          ###
    #############################
    
    hdmap_msgs/Point[] pts
    
    #############################
    int8 is_main # Is the lane center lane of this section
    float64 offset
    float64 width
    
    #############################
    
    #int32 id
    std_msgs/String uuid
    
    #############################
    
    std_msgs/String main_uuid
    
    #int32 left_id
    std_msgs/String left_uuid
    
    #int32 right_id
    std_msgs/String right_uuid
    
    
    #############################
    
    #int32 section_id
    std_msgs/String section_uuid
    
    #############################
    ================================================================================
    MSG: hdmap_msgs/Point
    #############################
    ###       SunHao          ###
    #############################
    
    float64 x
    float64 y
    float64 z
    float64 s
    float64 cuv
    float64 heading
    float64 speed
    float64 speedkmh
    
    ############################
    
    int32 speed_mode  # 速度模式["1:ultra_high", "2:high", "3:high_medium", "4:medium", "5:medium_low", "6:low", "7:ultra_low"]
    int32 obs_strategy  # 障碍物处理策略["1:follow", "2:left_chage_lane", "3:right_change_lane", "4:vfh", "5:blind"]
    int32 follow_strategy  # 循迹优先级["1:rtk-radar-video", "2:rtk-video-radar", "3:video-radar-rtk", "4:video-rtk-radar","5:radar-rtk-video", "6:radar-rtk-video"]
    int32 special_mode  # 特殊属性["1:normal", "2:merge_in", "3:backward", "4:short_wait", "5:longwait", "6:endpoint"]
    int32 obs_search_strategy  # 搜索障碍物策略["1:normal", "2:wide", "3:very_wide", "4:narrow", "5:very_narrow"]
    int32 cross_option  # 路口属性策略["1:not cross", "2:no trafficlight", "3:trafficlight"]
    int32 reserved_option  # 保留字段
    
    ############################
    
    #int32 id
    std_msgs/String uuid
    
    #############################
    
    #int32 line_id
    std_msgs/String lane_uuid
    
    #############################
    
    #int32 section_id
    std_msgs/String section_uuid
    
    ================================================================================
    MSG: std_msgs/String
    string data
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Map(null);
    if (msg.sections !== undefined) {
      resolved.sections = new Array(msg.sections.length);
      for (let i = 0; i < resolved.sections.length; ++i) {
        resolved.sections[i] = Section.Resolve(msg.sections[i]);
      }
    }
    else {
      resolved.sections = []
    }

    if (msg.name !== undefined) {
      resolved.name = std_msgs.msg.String.Resolve(msg.name)
    }
    else {
      resolved.name = new std_msgs.msg.String()
    }

    if (msg.utm_origin_x !== undefined) {
      resolved.utm_origin_x = msg.utm_origin_x;
    }
    else {
      resolved.utm_origin_x = 0.0
    }

    if (msg.utm_origin_y !== undefined) {
      resolved.utm_origin_y = msg.utm_origin_y;
    }
    else {
      resolved.utm_origin_y = 0.0
    }

    if (msg.utm_origin_z !== undefined) {
      resolved.utm_origin_z = msg.utm_origin_z;
    }
    else {
      resolved.utm_origin_z = 0.0
    }

    return resolved;
    }
};

module.exports = Map;
