
"use strict";

let PSAT = require('./PSAT.js');
let GTIMU = require('./GTIMU.js');
let GPGGA = require('./GPGGA.js');
let GPVTG = require('./GPVTG.js');
let GPRMC = require('./GPRMC.js');
let VehStat = require('./VehStat.js');
let GPFPD = require('./GPFPD.js');

module.exports = {
  PSAT: PSAT,
  GTIMU: GTIMU,
  GPGGA: GPGGA,
  GPVTG: GPVTG,
  GPRMC: GPRMC,
  VehStat: VehStat,
  GPFPD: GPFPD,
};
