; Auto-generated. Do not edit!


(cl:in-package hdmap_msgs-msg)


;//! \htmlinclude Map.msg.html

(cl:defclass <Map> (roslisp-msg-protocol:ros-message)
  ((sections
    :reader sections
    :initarg :sections
    :type (cl:vector hdmap_msgs-msg:Section)
   :initform (cl:make-array 0 :element-type 'hdmap_msgs-msg:Section :initial-element (cl:make-instance 'hdmap_msgs-msg:Section)))
   (name
    :reader name
    :initarg :name
    :type std_msgs-msg:String
    :initform (cl:make-instance 'std_msgs-msg:String))
   (utm_origin_x
    :reader utm_origin_x
    :initarg :utm_origin_x
    :type cl:float
    :initform 0.0)
   (utm_origin_y
    :reader utm_origin_y
    :initarg :utm_origin_y
    :type cl:float
    :initform 0.0)
   (utm_origin_z
    :reader utm_origin_z
    :initarg :utm_origin_z
    :type cl:float
    :initform 0.0))
)

(cl:defclass Map (<Map>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Map>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Map)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hdmap_msgs-msg:<Map> is deprecated: use hdmap_msgs-msg:Map instead.")))

(cl:ensure-generic-function 'sections-val :lambda-list '(m))
(cl:defmethod sections-val ((m <Map>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hdmap_msgs-msg:sections-val is deprecated.  Use hdmap_msgs-msg:sections instead.")
  (sections m))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <Map>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hdmap_msgs-msg:name-val is deprecated.  Use hdmap_msgs-msg:name instead.")
  (name m))

(cl:ensure-generic-function 'utm_origin_x-val :lambda-list '(m))
(cl:defmethod utm_origin_x-val ((m <Map>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hdmap_msgs-msg:utm_origin_x-val is deprecated.  Use hdmap_msgs-msg:utm_origin_x instead.")
  (utm_origin_x m))

(cl:ensure-generic-function 'utm_origin_y-val :lambda-list '(m))
(cl:defmethod utm_origin_y-val ((m <Map>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hdmap_msgs-msg:utm_origin_y-val is deprecated.  Use hdmap_msgs-msg:utm_origin_y instead.")
  (utm_origin_y m))

(cl:ensure-generic-function 'utm_origin_z-val :lambda-list '(m))
(cl:defmethod utm_origin_z-val ((m <Map>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hdmap_msgs-msg:utm_origin_z-val is deprecated.  Use hdmap_msgs-msg:utm_origin_z instead.")
  (utm_origin_z m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Map>) ostream)
  "Serializes a message object of type '<Map>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'sections))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'sections))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'name) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'utm_origin_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'utm_origin_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'utm_origin_z))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Map>) istream)
  "Deserializes a message object of type '<Map>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'sections) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'sections)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'hdmap_msgs-msg:Section))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'name) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'utm_origin_x) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'utm_origin_y) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'utm_origin_z) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Map>)))
  "Returns string type for a message object of type '<Map>"
  "hdmap_msgs/Map")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Map)))
  "Returns string type for a message object of type 'Map"
  "hdmap_msgs/Map")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Map>)))
  "Returns md5sum for a message object of type '<Map>"
  "46b057bbed8964803a46c0c17fd16115")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Map)))
  "Returns md5sum for a message object of type 'Map"
  "46b057bbed8964803a46c0c17fd16115")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Map>)))
  "Returns full string definition for message of type '<Map>"
  (cl:format cl:nil "#############################~%###       SunHao          ###~%#############################~%~%hdmap_msgs/Section[] sections~%~%#########################~%~%std_msgs/String name~%~%float64 utm_origin_x~%float64 utm_origin_y~%float64 utm_origin_z~%~%~%================================================================================~%MSG: hdmap_msgs/Section~%#############################~%###       SunHao          ###~%#############################~%~%~%~%hdmap_msgs/Lane[] lanes~%~%int32 direction_mode~%~%#############################~%#int32 id~%std_msgs/String uuid~%~%#############################~%~%std_msgs/String[] to_section_uuid~%std_msgs/String[] from_section_uuid~%~%~%std_msgs/String[] to_lane_uuid~%std_msgs/String[] from_lane_uuid~%~%~%std_msgs/String[] to_pt_uuid~%std_msgs/String[] from_pt_uuid~%~%#int32[] to_pt_id~%#int32[] from_pt_id~%~%#############################~%~%float64 length~%float64 highest_speed~%float64 traffic_cost~%================================================================================~%MSG: hdmap_msgs/Lane~%#############################~%###       SunHao          ###~%#############################~%~%hdmap_msgs/Point[] pts~%~%#############################~%int8 is_main # Is the lane center lane of this section~%float64 offset~%float64 width~%~%#############################~%~%#int32 id~%std_msgs/String uuid~%~%#############################~%~%std_msgs/String main_uuid~%~%#int32 left_id~%std_msgs/String left_uuid~%~%#int32 right_id~%std_msgs/String right_uuid~%~%~%#############################~%~%#int32 section_id~%std_msgs/String section_uuid~%~%#############################~%================================================================================~%MSG: hdmap_msgs/Point~%#############################~%###       SunHao          ###~%#############################~%~%float64 x~%float64 y~%float64 z~%float64 s~%float64 cuv~%float64 heading~%float64 speed~%float64 speedkmh~%~%############################~%~%int32 speed_mode  # 速度模式[\"1:ultra_high\", \"2:high\", \"3:high_medium\", \"4:medium\", \"5:medium_low\", \"6:low\", \"7:ultra_low\"]~%int32 obs_strategy  # 障碍物处理策略[\"1:follow\", \"2:left_chage_lane\", \"3:right_change_lane\", \"4:vfh\", \"5:blind\"]~%int32 follow_strategy  # 循迹优先级[\"1:rtk-radar-video\", \"2:rtk-video-radar\", \"3:video-radar-rtk\", \"4:video-rtk-radar\",\"5:radar-rtk-video\", \"6:radar-rtk-video\"]~%int32 special_mode  # 特殊属性[\"1:normal\", \"2:merge_in\", \"3:backward\", \"4:short_wait\", \"5:longwait\", \"6:endpoint\"]~%int32 obs_search_strategy  # 搜索障碍物策略[\"1:normal\", \"2:wide\", \"3:very_wide\", \"4:narrow\", \"5:very_narrow\"]~%int32 cross_option  # 路口属性策略[\"1:not cross\", \"2:no trafficlight\", \"3:trafficlight\"]~%int32 reserved_option  # 保留字段~%~%############################~%~%#int32 id~%std_msgs/String uuid~%~%#############################~%~%#int32 line_id~%std_msgs/String lane_uuid~%~%#############################~%~%#int32 section_id~%std_msgs/String section_uuid~%~%================================================================================~%MSG: std_msgs/String~%string data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Map)))
  "Returns full string definition for message of type 'Map"
  (cl:format cl:nil "#############################~%###       SunHao          ###~%#############################~%~%hdmap_msgs/Section[] sections~%~%#########################~%~%std_msgs/String name~%~%float64 utm_origin_x~%float64 utm_origin_y~%float64 utm_origin_z~%~%~%================================================================================~%MSG: hdmap_msgs/Section~%#############################~%###       SunHao          ###~%#############################~%~%~%~%hdmap_msgs/Lane[] lanes~%~%int32 direction_mode~%~%#############################~%#int32 id~%std_msgs/String uuid~%~%#############################~%~%std_msgs/String[] to_section_uuid~%std_msgs/String[] from_section_uuid~%~%~%std_msgs/String[] to_lane_uuid~%std_msgs/String[] from_lane_uuid~%~%~%std_msgs/String[] to_pt_uuid~%std_msgs/String[] from_pt_uuid~%~%#int32[] to_pt_id~%#int32[] from_pt_id~%~%#############################~%~%float64 length~%float64 highest_speed~%float64 traffic_cost~%================================================================================~%MSG: hdmap_msgs/Lane~%#############################~%###       SunHao          ###~%#############################~%~%hdmap_msgs/Point[] pts~%~%#############################~%int8 is_main # Is the lane center lane of this section~%float64 offset~%float64 width~%~%#############################~%~%#int32 id~%std_msgs/String uuid~%~%#############################~%~%std_msgs/String main_uuid~%~%#int32 left_id~%std_msgs/String left_uuid~%~%#int32 right_id~%std_msgs/String right_uuid~%~%~%#############################~%~%#int32 section_id~%std_msgs/String section_uuid~%~%#############################~%================================================================================~%MSG: hdmap_msgs/Point~%#############################~%###       SunHao          ###~%#############################~%~%float64 x~%float64 y~%float64 z~%float64 s~%float64 cuv~%float64 heading~%float64 speed~%float64 speedkmh~%~%############################~%~%int32 speed_mode  # 速度模式[\"1:ultra_high\", \"2:high\", \"3:high_medium\", \"4:medium\", \"5:medium_low\", \"6:low\", \"7:ultra_low\"]~%int32 obs_strategy  # 障碍物处理策略[\"1:follow\", \"2:left_chage_lane\", \"3:right_change_lane\", \"4:vfh\", \"5:blind\"]~%int32 follow_strategy  # 循迹优先级[\"1:rtk-radar-video\", \"2:rtk-video-radar\", \"3:video-radar-rtk\", \"4:video-rtk-radar\",\"5:radar-rtk-video\", \"6:radar-rtk-video\"]~%int32 special_mode  # 特殊属性[\"1:normal\", \"2:merge_in\", \"3:backward\", \"4:short_wait\", \"5:longwait\", \"6:endpoint\"]~%int32 obs_search_strategy  # 搜索障碍物策略[\"1:normal\", \"2:wide\", \"3:very_wide\", \"4:narrow\", \"5:very_narrow\"]~%int32 cross_option  # 路口属性策略[\"1:not cross\", \"2:no trafficlight\", \"3:trafficlight\"]~%int32 reserved_option  # 保留字段~%~%############################~%~%#int32 id~%std_msgs/String uuid~%~%#############################~%~%#int32 line_id~%std_msgs/String lane_uuid~%~%#############################~%~%#int32 section_id~%std_msgs/String section_uuid~%~%================================================================================~%MSG: std_msgs/String~%string data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Map>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'sections) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'name))
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Map>))
  "Converts a ROS message object to a list"
  (cl:list 'Map
    (cl:cons ':sections (sections msg))
    (cl:cons ':name (name msg))
    (cl:cons ':utm_origin_x (utm_origin_x msg))
    (cl:cons ':utm_origin_y (utm_origin_y msg))
    (cl:cons ':utm_origin_z (utm_origin_z msg))
))
