; Auto-generated. Do not edit!


(cl:in-package hdmap_msgs-msg)


;//! \htmlinclude HDMapPath.msg.html

(cl:defclass <HDMapPath> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (path
    :reader path
    :initarg :path
    :type (cl:vector hdmap_msgs-msg:Point)
   :initform (cl:make-array 0 :element-type 'hdmap_msgs-msg:Point :initial-element (cl:make-instance 'hdmap_msgs-msg:Point)))
   (left_lane
    :reader left_lane
    :initarg :left_lane
    :type (cl:vector hdmap_msgs-msg:Point)
   :initform (cl:make-array 0 :element-type 'hdmap_msgs-msg:Point :initial-element (cl:make-instance 'hdmap_msgs-msg:Point)))
   (right_lane
    :reader right_lane
    :initarg :right_lane
    :type (cl:vector hdmap_msgs-msg:Point)
   :initform (cl:make-array 0 :element-type 'hdmap_msgs-msg:Point :initial-element (cl:make-instance 'hdmap_msgs-msg:Point))))
)

(cl:defclass HDMapPath (<HDMapPath>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <HDMapPath>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'HDMapPath)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hdmap_msgs-msg:<HDMapPath> is deprecated: use hdmap_msgs-msg:HDMapPath instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <HDMapPath>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hdmap_msgs-msg:header-val is deprecated.  Use hdmap_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'path-val :lambda-list '(m))
(cl:defmethod path-val ((m <HDMapPath>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hdmap_msgs-msg:path-val is deprecated.  Use hdmap_msgs-msg:path instead.")
  (path m))

(cl:ensure-generic-function 'left_lane-val :lambda-list '(m))
(cl:defmethod left_lane-val ((m <HDMapPath>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hdmap_msgs-msg:left_lane-val is deprecated.  Use hdmap_msgs-msg:left_lane instead.")
  (left_lane m))

(cl:ensure-generic-function 'right_lane-val :lambda-list '(m))
(cl:defmethod right_lane-val ((m <HDMapPath>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hdmap_msgs-msg:right_lane-val is deprecated.  Use hdmap_msgs-msg:right_lane instead.")
  (right_lane m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <HDMapPath>) ostream)
  "Serializes a message object of type '<HDMapPath>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'path))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'path))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'left_lane))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'left_lane))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'right_lane))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'right_lane))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <HDMapPath>) istream)
  "Deserializes a message object of type '<HDMapPath>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'path) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'path)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'hdmap_msgs-msg:Point))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'left_lane) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'left_lane)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'hdmap_msgs-msg:Point))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'right_lane) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'right_lane)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'hdmap_msgs-msg:Point))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<HDMapPath>)))
  "Returns string type for a message object of type '<HDMapPath>"
  "hdmap_msgs/HDMapPath")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'HDMapPath)))
  "Returns string type for a message object of type 'HDMapPath"
  "hdmap_msgs/HDMapPath")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<HDMapPath>)))
  "Returns md5sum for a message object of type '<HDMapPath>"
  "d4beb0be1c23307204027f62a58ce27f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'HDMapPath)))
  "Returns md5sum for a message object of type 'HDMapPath"
  "d4beb0be1c23307204027f62a58ce27f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<HDMapPath>)))
  "Returns full string definition for message of type '<HDMapPath>"
  (cl:format cl:nil "#############################~%###       SunHao          ###~%#############################~%~%~%std_msgs/Header header~%~%hdmap_msgs/Point[] path~%hdmap_msgs/Point[] left_lane~%hdmap_msgs/Point[] right_lane~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: hdmap_msgs/Point~%#############################~%###       SunHao          ###~%#############################~%~%float64 x~%float64 y~%float64 z~%float64 s~%float64 cuv~%float64 heading~%float64 speed~%float64 speedkmh~%~%############################~%~%int32 speed_mode  # 速度模式[\"1:ultra_high\", \"2:high\", \"3:high_medium\", \"4:medium\", \"5:medium_low\", \"6:low\", \"7:ultra_low\"]~%int32 obs_strategy  # 障碍物处理策略[\"1:follow\", \"2:left_chage_lane\", \"3:right_change_lane\", \"4:vfh\", \"5:blind\"]~%int32 follow_strategy  # 循迹优先级[\"1:rtk-radar-video\", \"2:rtk-video-radar\", \"3:video-radar-rtk\", \"4:video-rtk-radar\",\"5:radar-rtk-video\", \"6:radar-rtk-video\"]~%int32 special_mode  # 特殊属性[\"1:normal\", \"2:merge_in\", \"3:backward\", \"4:short_wait\", \"5:longwait\", \"6:endpoint\"]~%int32 obs_search_strategy  # 搜索障碍物策略[\"1:normal\", \"2:wide\", \"3:very_wide\", \"4:narrow\", \"5:very_narrow\"]~%int32 cross_option  # 路口属性策略[\"1:not cross\", \"2:no trafficlight\", \"3:trafficlight\"]~%int32 reserved_option  # 保留字段~%~%############################~%~%#int32 id~%std_msgs/String uuid~%~%#############################~%~%#int32 line_id~%std_msgs/String lane_uuid~%~%#############################~%~%#int32 section_id~%std_msgs/String section_uuid~%~%================================================================================~%MSG: std_msgs/String~%string data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'HDMapPath)))
  "Returns full string definition for message of type 'HDMapPath"
  (cl:format cl:nil "#############################~%###       SunHao          ###~%#############################~%~%~%std_msgs/Header header~%~%hdmap_msgs/Point[] path~%hdmap_msgs/Point[] left_lane~%hdmap_msgs/Point[] right_lane~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: hdmap_msgs/Point~%#############################~%###       SunHao          ###~%#############################~%~%float64 x~%float64 y~%float64 z~%float64 s~%float64 cuv~%float64 heading~%float64 speed~%float64 speedkmh~%~%############################~%~%int32 speed_mode  # 速度模式[\"1:ultra_high\", \"2:high\", \"3:high_medium\", \"4:medium\", \"5:medium_low\", \"6:low\", \"7:ultra_low\"]~%int32 obs_strategy  # 障碍物处理策略[\"1:follow\", \"2:left_chage_lane\", \"3:right_change_lane\", \"4:vfh\", \"5:blind\"]~%int32 follow_strategy  # 循迹优先级[\"1:rtk-radar-video\", \"2:rtk-video-radar\", \"3:video-radar-rtk\", \"4:video-rtk-radar\",\"5:radar-rtk-video\", \"6:radar-rtk-video\"]~%int32 special_mode  # 特殊属性[\"1:normal\", \"2:merge_in\", \"3:backward\", \"4:short_wait\", \"5:longwait\", \"6:endpoint\"]~%int32 obs_search_strategy  # 搜索障碍物策略[\"1:normal\", \"2:wide\", \"3:very_wide\", \"4:narrow\", \"5:very_narrow\"]~%int32 cross_option  # 路口属性策略[\"1:not cross\", \"2:no trafficlight\", \"3:trafficlight\"]~%int32 reserved_option  # 保留字段~%~%############################~%~%#int32 id~%std_msgs/String uuid~%~%#############################~%~%#int32 line_id~%std_msgs/String lane_uuid~%~%#############################~%~%#int32 section_id~%std_msgs/String section_uuid~%~%================================================================================~%MSG: std_msgs/String~%string data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <HDMapPath>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'path) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'left_lane) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'right_lane) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <HDMapPath>))
  "Converts a ROS message object to a list"
  (cl:list 'HDMapPath
    (cl:cons ':header (header msg))
    (cl:cons ':path (path msg))
    (cl:cons ':left_lane (left_lane msg))
    (cl:cons ':right_lane (right_lane msg))
))
