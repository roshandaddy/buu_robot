(cl:in-package hdmap_msgs-msg)
(cl:export '(SECTIONS-VAL
          SECTIONS
          NAME-VAL
          NAME
          UTM_ORIGIN_X-VAL
          UTM_ORIGIN_X
          UTM_ORIGIN_Y-VAL
          UTM_ORIGIN_Y
          UTM_ORIGIN_Z-VAL
          UTM_ORIGIN_Z
))