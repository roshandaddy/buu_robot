(cl:in-package hdmap_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          PATH-VAL
          PATH
          LEFT_LANE-VAL
          LEFT_LANE
          RIGHT_LANE-VAL
          RIGHT_LANE
))